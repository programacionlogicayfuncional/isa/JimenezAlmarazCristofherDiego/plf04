(ns plf04.core)

(defn string-e-1
  [s]
  (letfn [(g [s s1]
            (cond
              (and (= s \e) (>= (s1 :c) 3)) {:r false :c (inc (s1 :c))}
              (and (= s \e) (>= (s1 :c) 0)) {:r true :c (inc (s1 :c))}
              :else s1))
          (f [xs]
            (if (empty? xs)
              {:r false :c 0}
              (g (first xs) (f (rest xs)))))]
    ((f s) :r)))

(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hll")
(string-e-1 "e")
(string-e-1 "")

(defn string-e-2
  [s]
  (letfn [(g [x]
            (and (>= x 1) (<= x 3)))
          (h [x y]
            (if (= x \e) (inc y) y))
          (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (rest xs)
                 (h (first xs) acc))))]
    (f s 0)))

(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hll")
(string-e-2 "e")
(string-e-2 "")


(defn string-times-1
  [s n]
  (if (== n 0)
    ""
    (str s (string-times-1 s (dec n)))))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [s n acc]
  (if (== n 0)
    acc
    (string-times-2 s (dec n) (str s acc))))

(string-times-2 "Hi" 2 "")
(string-times-2 "Hi" 3 "")
(string-times-2 "Hi" 1 "")
(string-times-2 "Hi" 0 "")
(string-times-2 "Hi" 5 "")
(string-times-2 "Oh Boy!" 2 "")
(string-times-2 "x" 4 "")
(string-times-2 "" 4 "")
(string-times-2 "code" 2 "")
(string-times-2 "code" 3 "")


(defn front-times-1
  [s n]
  (letfn [(f [x y]
            (cond (< (count x) 3)
                  (if (zero? y)
                    ""
                    (str s (f x (dec y))))
                  (zero? y)
                  ""
                  :else (str (first x) (first (rest x)) (first (rest (rest x))) (f x (dec y)))))]
    (f s n)))


(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [cadena n]
  (letfn [(f [x y w]
            (cond (< (count x) 3)
                  (if (zero? y)
                    w
                    (f x (dec y) (str x w)))
                  (zero? y)
                  w
                  :else (f x (dec y) (str (first x) (first (rest x)) (first (rest (rest x))) w))))]
    (f cadena n "")))

(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)


(defn count-x-x-1
  [s]
  (if (empty? s)
    s
    (let [count-x-x-1 (fn [p] (= p [\x \x]))]
      (count (filter count-x-x-1 (map vector s (rest s)))))))

(count-x-x-1 "abcxx")
(count-x-x-1 "xxx")
(count-x-x-1 "xxxx")
(count-x-x-1 "abc")
(count-x-x-1 "Hello there")
(count-x-x-1 "Hexxo thxxe")
(count-x-x-1 " ")
(count-x-x-1 "Kittens")
(count-x-x-1 "Kittensxxx")


(defn count-x-x-2
  [s acc]
  (if (empty? s)
    acc
    (+ acc (let [count-x-x-1 (fn [p] (= p [\x \x]))]
             (count (filter count-x-x-1 (map vector s (rest s))))))))

(count-x-x-2 "abcxx" 0)
(count-x-x-2 "xxx" 0)
(count-x-x-2 "xxxx" 0)
(count-x-x-2 "abc" 0)
(count-x-x-2 "Hello there" 0)
(count-x-x-2 "Hexxo thxxe" 0)
(count-x-x-2 "" 0)
(count-x-x-2 "Kittens" 0)
(count-x-x-2 "Kittensxxx" 0)


(defn string-splosion-1
  [s]
  (if (== 0 (count s))
    s
    (apply str (string-splosion-1 (subs s 0 (- (count s) 1))) s)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [s acc]
  (if (== 0 (count s))
    acc
    (string-splosion-2 (subs s 0 (- (count s) 1)) (str s acc))))

(string-splosion-2 "Code" "")
(string-splosion-2 "abc" "")
(string-splosion-2 "ab" "")
(string-splosion-2 "x" "")
(string-splosion-2 "fade" "")
(string-splosion-2 "There" "")
(string-splosion-2 "Kitten" "")
(string-splosion-2 "Bye" "")
(string-splosion-2 "Good" "")
(string-splosion-2 "Bad" "")


(defn array-1-2-3-1
  [xs]
  (letfn [(f [xs]
            (if (and (= 1 (first xs))
                     (= 2 (first (rest xs)))
                     (= 3 (first (rest (rest xs)))))
              true (if (empty? xs) false (f (rest xs)))))]

    (f xs)))

(array-1-2-3-1 [1,1,2,3,1])
(array-1-2-3-1 [1,1,2,4,1])
(array-1-2-3-1 [1,1,2,3,1])
(array-1-2-3-1 [1,1,2,1,2,3])
(array-1-2-3-1 [1,1,2,1,2,1])
(array-1-2-3-1 [1,2,3,1,2,3])
(array-1-2-3-1 [1,2,3])
(array-1-2-3-1 [1,1,1])
(array-1-2-3-1 [1,2])
(array-1-2-3-1 [1])
(array-1-2-3-1 [])

(defn array-1-2-3-2
  [xs]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (and acc false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  (and acc true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f xs "")))

(array-1-2-3-2 [1,1,2,3,1])
(array-1-2-3-2 [1,1,2,4,1])
(array-1-2-3-2 [1,1,2,3,1])
(array-1-2-3-2 [1,1,2,1,2,3])
(array-1-2-3-2 [1,1,2,1,2,1])
(array-1-2-3-2 [1,2,3,1,2,3])
(array-1-2-3-2 [1,2,3])
(array-1-2-3-2 [1,1,1])
(array-1-2-3-2 [1,2])
(array-1-2-3-2 [1])
(array-1-2-3-2 [])


(defn string-x-1
  [s]
  (letfn [(f [x y]
            (cond (empty? x)
                  ""
                  (= y 0)
                  (str (first x) (f (rest x) (inc y)))
                  :else (if (and (= \x (first x)) (> (count x) 1))
                          (f (rest x) (inc y))
                          (str (first x) (f (rest x) (inc y))))))]
    (f s 0)))


(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

(defn string-x-2
  [s]
  (letfn [(f [x y w]
            (cond (empty? x)
                  w
                  (= y 0)
                  (str (first x) (f (rest x) (inc y) (str w)))
                  :else (if (and (= \x (first x)) (> (count x) 1))
                          (f (rest x) (inc y) (str w))
                          (str (first x) (f (rest x) (inc y) w)))))]
    (f s 0 "")))

(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")


(defn altPairs-1
  [s]
  (letfn [(f [x y]
            (cond (== (count x) y)
                  ""
                  (or (== y 0) (== y 1) (== y 4) (== y 5) (== y 8) (== y 9))
                  (str (str (first (drop y x))) (f s (inc y)))
                  :else (f s (inc y))))]
    (f s 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
  [s]
  (letfn [(f [x y w]
            (cond (== (count x) y)
                  w
                  (or (== y 0) (== y 1) (== y 4) (== y 5) (== y 8) (== y 9))
                  (f x (inc y) (str w (str (first (drop y x)))))
                  :else (f x (inc y) w)))]
    (f s 0 "")))

(altPairs-2 "kitten")
(altPairs-2 "Chocolate")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOTher")

(defn string-yak-1
  [s]
  (letfn [(f [x z]
            (cond (empty? x)
                  ""
                  (and (= (first x) \y) (= (first (rest x)) \a) (= (first (rest (rest x))) \k) (> (count x) 1))
                  (f (rest (rest (rest x))) (inc z))
                  :else (str (first x) (f (rest x) (inc z)))))]
    (f s 0)))

(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

(defn string-yak-2
  [s]
  (letfn [(f [x y w]
            (cond (empty? x)
                  w
                  (and (= (first x) \y) (= (first (rest x)) \a) (= (first (rest (rest x))) \k) (> (count x) 1))
                  (f (rest (rest (rest x))) (inc y) w)
                  :else (f (rest x) (inc y) (str w (first x)))))]
    (f s 0 "")))


(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")


(defn has-271-1
  [xs]
  (letfn [(f [ys]
            (cond (or (<= (count ys) 2) (empty? ys))
                  false
                  (and
                   (== (first (rest ys)) (+ (first ys) 5))
                   (<= (if (pos? (- (first (rest (rest ys))) (dec (first ys))))
                         (- (first (rest (rest ys))) (dec (first ys)))
                         (* -1 (- (first (rest (rest ys))) (dec (first ys))))) 2))
                  true
                  :else (f (rest ys))))]
    (f xs)))

(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

(defn has-271-2
  [xs acc]
  (letfn [(f [ys y]
            (cond (>= (count ys) 3)
                  (if (and (== (- (first (rest ys)) (first ys)) 5)
                           (>= 2 (if (neg? (- (first (rest (rest ys))) (- (first ys) 1)))
                                   (* -1 (- (first (rest (rest ys))) (- (first ys) 1)))
                                   (* 1 (- (first (rest (rest ys))) (- (first ys) 1))))))
                    true
                    (f (rest ys) y))
                  (<= (count ys) 3)
                  false))]
    (f xs acc)))

(has-271-2 [1 2 7 1] "")
(has-271-2 [1 2 8 1] "")
(has-271-2 [2 7 1] "")
(has-271-2 [3 8 2] "")
(has-271-2 [2 7 3] "")
(has-271-2 [2 7 4] "")
(has-271-2 [2 7 -1] "")
(has-271-2 [2 7 -2] "")
(has-271-2 [4 5 3 8 0] "")
(has-271-2 [2 7 5 10 4] "")
(has-271-2 [2 7 -2 4 9 3] "")
(has-271-2 [2 7 5 10 1] "")
(has-271-2 [2 7 -2 4 10 2] "")
(has-271-2 [1 1 4 9 0] "")
(has-271-2 [1 1 4 9 4 9 2] "")